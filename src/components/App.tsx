import React, {useState} from 'react';
import classes from './App.module.scss';
import {Link, Outlet} from "react-router-dom";
import healthPng from "@/assets/health.png";
import bpbJpg from "@/assets/bpb1.jpg";
import BpbSvg from "@/assets/bpb2.svg"

// удаляется из бандла если не вызывается в дальнейшем
// это TREE SHAKING
function TODO() {
  TODO2();
}

function TODO2() {
  throw new Error();
}

export const App = () => {
  const [count, setCount] = useState<number>(0);

  const increment = () => {
    // setCount(prev => prev + 1);
    TODO();
  }
  // TODO('string');
  //
  // if (__PLATFORM__ === 'desktop') {
  //   return <div>IS DESKTOP PLATFORM</div>
  // }
  //
  // if (__PLATFORM__ === 'mobile') {
  //   return <div>IS MOBILE PLATFORM</div>
  // }

  return (
      <div>
        <h1 data-testid={'Platform'}>PLATFORM={__PLATFORM__}</h1>
        <div>
          <img width={100} src={healthPng} alt="#"/>
          <img width={100} src={bpbJpg} alt="#"/>
        </div>
        <div>
          <BpbSvg className={classes.icon} width={150} height={150}></BpbSvg>
        </div>
        <Link to={'/about'}>About</Link>
        <br/>
        <Link to={'/shop'}>Shop</Link>
        <h1 className={classes.value}>{count}</h1>
        <button className={classes.button} onClick={increment}>
          <span>Inc</span>
        </button>
        <Outlet/>
      </div>
  );
};

export default App;